# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias eucalyptus='mosh eucalyptus tmux a'
alias nvlc='nvlc --no-color --no-video'

PS1='[\u@\h \W]\$ '

if [[ -t 0 && $(tty) == /dev/tty1 ]]; then
    export PATH="/home/kai/.local/bin:$PATH"
    export NO_AT_BRIDGE=1
    #export QT_QPA_PLATFORM=wayland-egl
    #export CLUTTER_BACKEND=wayland
    #export SDL_VIDEODRIVER=wayland
    #export MOZ_ENABLE_WAYLAND=1
    #export XDG_SESSION_TYPE=wayland
    export XMODIFIERS DEFAULT=@im=fcitx
    export GTK_IM_MODULE=fcitx
    export QT_IM_MODULE=fcitx
    #exec dbus-run-session sway
    exec startx
fi
